package bee.bee.astrazeneca.util;

import android.content.Context;
import android.content.SharedPreferences;

public class Preferences {
    private String PREFS_NAME = "astraZenecaPref";
    private String USER_ID_PREF = "userID";
    private String USER_Email_PREF = "userEmail";
    private String USER_IMAGE_URL_PREF = "userImage";
    private String USER_FULL_NAME_PREF = "fullName";
    private String USER_PHONE_PREF = "userPhone";
    private String USER_Password_PREF = "userPassword";

    private SharedPreferences APP_PREFERENCE;
    // notifications preference
  //  private String APPLICATION_LOCAL_PREF = "ApplicationLocale";
    private String APPLICATION_VERSION_CODE_PREF = "ApplicationVersionCode";
    private SharedPreferences.Editor preferenceEditor;
    private static Preferences instance;

    public static Preferences getInstance() {
        if (instance == null)
            instance = new Preferences();
        return instance;
    }
    public void InitializePreferences(Context context) {
        APP_PREFERENCE = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }



   /* public String getApplicationLocale() {
        return APP_PREFERENCE.getString(APPLICATION_LOCAL_PREF, "");
    }

    public void SaveApplicationLocale(String local) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(APPLICATION_LOCAL_PREF, local);
        preferenceEditor.apply();
    }*/

    public void SaveApplicationVersionCode(String versionCode) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(APPLICATION_VERSION_CODE_PREF, versionCode);
        preferenceEditor.apply();
    }

    public String getApplicationVersionCode() {
        return APP_PREFERENCE.getString(APPLICATION_VERSION_CODE_PREF, "");
    }


    public String getUserPassword() {
        return APP_PREFERENCE.getString(USER_Password_PREF, "");
    }

    public void saveUserPassword(String userPassword) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(USER_Password_PREF, userPassword);
        preferenceEditor.apply();
    }


    public void saveUserImageUrl(String imageUrl) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(USER_IMAGE_URL_PREF, imageUrl);
        preferenceEditor.apply();
    }

    public String getUserImageUrl() {
        return APP_PREFERENCE.getString(USER_IMAGE_URL_PREF, "");
    }

    public void saveUserEmail(String userEmail) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(USER_Email_PREF, userEmail);
        preferenceEditor.apply();
    }




    public String getUserEmail() {
        return APP_PREFERENCE.getString(USER_Email_PREF, "");
    }


    public String getUserName() {
        return APP_PREFERENCE.getString(USER_FULL_NAME_PREF, "");
    }

    public void saveUserName(String fullName) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(USER_FULL_NAME_PREF, fullName);
        preferenceEditor.apply();
    }

    public String getUserPhone() {
        return APP_PREFERENCE.getString(USER_PHONE_PREF, "");
    }

    public void saveUserPhone(String userPhone) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(USER_PHONE_PREF, userPhone);
        preferenceEditor.apply();
    }



    public void saveUserID(String userID) {
        preferenceEditor = APP_PREFERENCE.edit();
        preferenceEditor.putString(USER_ID_PREF, userID);
        preferenceEditor.apply();
    }

    public String getUserID() {
        return APP_PREFERENCE.getString(USER_ID_PREF, "");
    }


    public void clearUserData() {
        saveUserID("");
        saveUserName("");
        saveUserEmail("");
        saveUserPhone("");
        saveUserImageUrl("");
    }
}
