package bee.bee.astrazeneca.adapter;


import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import bee.bee.astrazeneca.ui.fragments.MainFragment;
import bee.bee.astrazeneca.ui.fragments.ProductFragment;

public class ViewPagerBrilintaAdapter extends FragmentPagerAdapter {
    private int tabs;

    public ViewPagerBrilintaAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
        this.tabs = behavior;
    }


    @NonNull
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new MainFragment();
            case 1:
                return new ProductFragment();
            case 2:
                return new ProductFragment();
            case 3:
                return new ProductFragment();
            case 4:
                return new ProductFragment();
            case 5:
                return new ProductFragment();
            default:
                return null;
        }

    }

    @Override
    public int getCount() {
        return tabs;
    }

}
