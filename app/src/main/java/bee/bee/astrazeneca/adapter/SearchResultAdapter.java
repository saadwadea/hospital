package bee.bee.astrazeneca.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.pojo.SearchData;

public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.ViewHolder> {
    private Context mContext;
    private SearchData[] res;
    private OnItemClickListener mListener;

    public SearchResultAdapter(Context context, SearchData[] data) {
        this.mContext = context;
        this.res=data;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }


    @NonNull
    @Override
    public SearchResultAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.result_search_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final SearchResultAdapter.ViewHolder holder, int position) {
        SearchData data=res[position];
        holder.txTitle.setText(data.getTitle());
        holder.txDate.setText(data.getDate());
        holder.txSearchTitle.setText(data.getTitleSearch());
        holder.txSearchMedicine.setText(data.getTitleMedicine());
    }

    @Override
    public int getItemCount() {
        return res.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txTitle,txDate,txSearchTitle,txSearchMedicine;
        public ViewHolder(View itemView) {
            super(itemView);

            txTitle=itemView.findViewById(R.id.txTitle);
            txDate=itemView.findViewById(R.id.txDate);
            txSearchMedicine=itemView.findViewById(R.id.txSearchMedicine);
            txSearchTitle=itemView.findViewById(R.id.txSearchTitle);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);

                        }
                    }
                }
            });
        }
    }

    public void setList(SearchData[]newList){
        this.res=newList;
        notifyDataSetChanged();

    }

}


