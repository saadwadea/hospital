package bee.bee.astrazeneca.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.pojo.RecordedVideoData;

public class RecordedVideoAdapter extends RecyclerView.Adapter<RecordedVideoAdapter.ViewHolder> {
    private Context mContext;
    private RecordedVideoData []res;
    private OnItemClickListener mListener;

    public RecordedVideoAdapter(Context context,RecordedVideoData[] data) {
        this.mContext = context;
        this.res=data;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }


    @NonNull
    @Override
    public RecordedVideoAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.recorded_video_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final RecordedVideoAdapter.ViewHolder holder, int position) {
        RecordedVideoData data=res[position];
        holder.imageView.setImageResource(data.getImage());
        holder.txTitle.setText(data.getTitle());
        holder.txName.setText(data.getName());
    }

    @Override
    public int getItemCount() {
        return res.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txTitle,txName;
        ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            txTitle=itemView.findViewById(R.id.txTitle);
            txName=itemView.findViewById(R.id.txName);
            imageView=itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);

                        }
                    }
                }
            });
        }
    }

    public void setList(RecordedVideoData[]newList){
        this.res=newList;
        notifyDataSetChanged();

    }

}


