package bee.bee.astrazeneca.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.makeramen.roundedimageview.RoundedImageView;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.ApprovalData;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.fragments.SFDAProgramFragment;
import bee.bee.astrazeneca.ui.fragments.TherapyFragment;

public class ApprovalAdapter extends RecyclerView.Adapter<ApprovalAdapter.ViewHolder> {
    private MainActivity mContext;
    private ApprovalData[] res;

    public ApprovalAdapter(MainActivity context, ApprovalData[] approvalData) {
        this.mContext = context;
        this.res=approvalData;
    }



    @NonNull
    @Override
    public ApprovalAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.approval_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ApprovalAdapter.ViewHolder holder, int position) {
        ApprovalData data=res[position];
        holder.imageView.setImageResource(data.getImage());
        holder.txTitle.setText(data.getTitle());
    }

    @Override
    public int getItemCount() {
        return res.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txTitle;
        RoundedImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            txTitle=itemView.findViewById(R.id.txTitle);
            imageView=itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mContext.replaceCurrentFragment(R.id.main_fragment,
                            new SFDAProgramFragment(), "SFDAProgramFragment", true, true);
                        int position = getAdapterPosition();

                }
            });
        }
    }

    public void setList(ApprovalData[]newList){
        this.res=newList;
        notifyDataSetChanged();

    }

}


