package bee.bee.astrazeneca.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.joooonho.SelectableRoundedImageView;

import java.util.ArrayList;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;

public class UsefulMaterialAdapter extends RecyclerView.Adapter<UsefulMaterialAdapter.ViewHolder> {
    private Context mContext;
    private MaterialData[] res;
    private OnItemClickListener mListener;

    public UsefulMaterialAdapter(Context context,MaterialData[] materialData) {
        this.mContext = context;
        this.res=materialData;
    }


    public interface OnItemClickListener {
        void onItemClick(int position);
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mListener = listener;
    }


    @NonNull
    @Override
    public UsefulMaterialAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.useful_material_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final UsefulMaterialAdapter.ViewHolder holder, int position) {
        MaterialData data=res[position];
        holder.imageView.setImageResource(data.getImage());
        holder.txTitle.setText(data.getTitle());
        holder.txDate.setText(data.getDate());
    }

    @Override
    public int getItemCount() {
        return res.length;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txTitle,txDate;
        ImageView imageView;
        public ViewHolder(View itemView) {
            super(itemView);

            txTitle=itemView.findViewById(R.id.txTitle);
            txDate=itemView.findViewById(R.id.txDate);
            imageView=itemView.findViewById(R.id.image);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        int position = getAdapterPosition();
                        if (position != RecyclerView.NO_POSITION) {
                            mListener.onItemClick(position);

                        }
                    }
                }
            });
        }
    }

    public void setList(MaterialData[]newList){
        this.res=newList;
        notifyDataSetChanged();

    }

}


