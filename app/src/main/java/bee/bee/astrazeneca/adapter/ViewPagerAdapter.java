package bee.bee.astrazeneca.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.makeramen.roundedimageview.RoundedImageView;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.HomeViewPagerData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.fragments.TherapyFragment;

public class ViewPagerAdapter extends PagerAdapter {
    private MainActivity context;
    private LayoutInflater layoutInflater;
    private HomeViewPagerData[] res;


    public ViewPagerAdapter(MainActivity context,HomeViewPagerData[] pagerData) {
        this.context = context;
        this.res=pagerData;
    }


    @Override
    public int getCount() {
        return res.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull final ViewGroup container, final int position) {

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.viewpager_top_home_item, null);
        HomeViewPagerData data=res[position];
        RoundedImageView imageView = view.findViewById(R.id.imageView);
        TextView textView = view.findViewById(R.id.text);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setImageResource(data.getImage());
        textView.setText(data.getText());
        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        view.setOnClickListener(v -> context.replaceCurrentFragment(R.id.main_fragment,
                new TherapyFragment(), "TherapyFragment", true, true));
        return view;

    }


    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);
    }
}