package bee.bee.astrazeneca.pojo;

public class RecordedVideoData {
    private int image;
    private String title;
    private String name;

    public RecordedVideoData(int image, String title, String name) {
        this.image = image;
        this.title = title;
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public RecordedVideoData setImage(int image) {
        this.image = image;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public RecordedVideoData setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getName() {
        return name;
    }

    public RecordedVideoData setName(String name) {
        this.name = name;
        return this;
    }
}
