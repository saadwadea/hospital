package bee.bee.astrazeneca.pojo;

public class ApprovalData {
    private int image;
    private String title;

    public ApprovalData(int image, String title) {
        this.image = image;
        this.title = title;
    }
    public ApprovalData() {
    }

    public int getImage() {
        return image;
    }

    public ApprovalData setImage(int image) {
        this.image = image;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public ApprovalData setTitle(String title) {
        this.title = title;
        return this;
    }

}
