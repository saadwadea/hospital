package bee.bee.astrazeneca.pojo;

public class HomeViewPagerData {
    private String text;
    private int image;

    public HomeViewPagerData(String text, int image) {
        this.text = text;
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public HomeViewPagerData setText(String text) {
        this.text = text;
        return this;
    }

    public int getImage() {
        return image;
    }

    public HomeViewPagerData setImage(int image) {
        this.image = image;
        return this;
    }
}
