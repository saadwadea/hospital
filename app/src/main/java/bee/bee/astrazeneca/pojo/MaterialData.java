package bee.bee.astrazeneca.pojo;

public class MaterialData {
    private int image;
    private String title;
    private String date;

    public MaterialData(int image, String title, String date) {
        this.image = image;
        this.title = title;
        this.date = date;
    }
    public MaterialData() {
    }

    public int getImage() {
        return image;
    }

    public MaterialData setImage(int image) {
        this.image = image;
        return this;
    }

    public String getTitle() {
        return title;
    }

    public MaterialData setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDate() {
        return date;
    }

    public MaterialData setDate(String date) {
        this.date = date;
        return this;
    }
}
