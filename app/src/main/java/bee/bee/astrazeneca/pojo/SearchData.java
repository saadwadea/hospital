package bee.bee.astrazeneca.pojo;

public class SearchData {
   private String title;
    private String date;
    private String titleSearch;
    private String titleMedicine;

    public SearchData(String title, String date, String titleSearch, String titleMedicine) {
        this.title = title;
        this.date = date;
        this.titleSearch = titleSearch;
        this.titleMedicine = titleMedicine;
    }

    public String getTitle() {
        return title;
    }

    public SearchData setTitle(String title) {
        this.title = title;
        return this;
    }

    public String getDate() {
        return date;
    }

    public SearchData setDate(String date) {
        this.date = date;
        return this;
    }

    public String getTitleSearch() {
        return titleSearch;
    }

    public SearchData setTitleSearch(String titleSearch) {
        this.titleSearch = titleSearch;
        return this;
    }

    public String getTitleMedicine() {
        return titleMedicine;
    }

    public SearchData setTitleMedicine(String titleMedicine) {
        this.titleMedicine = titleMedicine;
        return this;
    }
}
