package bee.bee.astrazeneca.pojo;

public class LoginUser {
    private String Email;
    private String Password;

    public LoginUser(String email, String password) {
        Email = email;
        Password = password;
    }

    public String getEmail() {
        return Email;
    }

    public LoginUser setEmail(String email) {
        Email = email;
        return this;
    }

    public String getPassword() {
        return Password;
    }

    public LoginUser setPassword(String password) {
        Password = password;
        return this;
    }
}
