package bee.bee.astrazeneca.ui.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentProfileBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;

public class ProfileFragment extends Fragment {

    private FragmentProfileBinding fragmentProfileBinding;
    MainActivity activity;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            fragmentProfileBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, null, false);
            v = fragmentProfileBinding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        activity = (MainActivity) getActivity();
        setListener();
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.MyProfile));
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
        activity.showMenu();
        activity.hideBackImage();

    }
    private void setListener(){
        fragmentProfileBinding.btChangePassword.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new ChangePasswordFragment(), "ChangePasswordFragment", true, true));
    }
}
