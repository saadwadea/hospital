package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentMenuBinding;
import bee.bee.astrazeneca.ui.activities.LoginActivity;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.util.AnimationExpandableList;


public class MenuFragment extends Fragment {
    MainActivity activity;
    private FragmentMenuBinding menuBinding;
    View v;
    public static MenuFragment getInstance() {
        return new MenuFragment();
    }
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            menuBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, null, false);
            v = menuBinding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    private void setListener() {
        menuBinding.layoutUserSlideMenu.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new ProfileFragment(), "ProfileFragment", true, true);


        });

        menuBinding.homeContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,new HomeFragment(), "HomeFragment", true, true);
        });
        menuBinding.MaterialContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new UsefulMaterialsFragment(), "UsefulMaterialsFragment", true, true);
        });
        menuBinding.LiveContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new LiveFragment(), "LiveFragment", true, true);
        });
        menuBinding.ContactUsContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new ContactUsFragment(), "ContactUsFragment", true, true);
        });
        menuBinding.LogOutContainer.setOnClickListener(v -> {
            Intent intent=new Intent(activity, LoginActivity.class);
            startActivity(intent);
            activity.finish_activity();
        });
        menuBinding.TermsContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new TermsOfUseFragment(), "TermsOfUseFragment", true, true);
        });
        menuBinding.PrivacyContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new PrivacyFragment(), "PrivacyFragment", true, true);
        });
        menuBinding.VideoContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new RecordedVideoFragment(), "RecordedVideoFragment", true, true);
        });
        menuBinding.TherapyContainer.setOnClickListener(v -> {
            menuBinding.expandableContainer.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (menuBinding.expandableContainer.getVisibility()==View.GONE) {
                AnimationExpandableList.getInstance().expand(menuBinding.expandableContainer);
                menuBinding.imageArrow.setImageResource(R.drawable.menu_drop_down_up_icon);
            }
            else
            {
                AnimationExpandableList.getInstance().collapse(menuBinding.expandableContainer);
                menuBinding.imageArrow.setImageResource(R.drawable.menu_drop_down_down_icon);

            }
        });
        menuBinding.txtCardiovascular.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new TherapyFragment(), "TherapyFragment", true, true);
        });
        menuBinding.txtDiabetes.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new TherapyFragment(), "TherapyFragment", true, true);
        });menuBinding.txtGastrointestinal.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new TherapyFragment(), "TherapyFragment", true, true);
        });menuBinding.txtOncology.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new TherapyFragment(), "TherapyFragment", true, true);
        });
        menuBinding.txtRespiratory.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new TherapyFragment(), "TherapyFragment", true, true);

        });
        menuBinding.ReportContainer.setOnClickListener(v -> {
            activity.closeMenu();
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new ReportFragment(), "ReportFragment", true, true);
        });
    }
}
