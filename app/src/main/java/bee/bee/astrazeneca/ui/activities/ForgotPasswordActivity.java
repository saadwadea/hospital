package bee.bee.astrazeneca.ui.activities;

import androidx.databinding.DataBindingUtil;
import android.os.Bundle;
import android.widget.Toast;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.ActivityForgotPasswordBinding;
import bee.bee.astrazeneca.util.EmailValidator;

public class ForgotPasswordActivity extends BaseActivity {
    ActivityForgotPasswordBinding binding;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_forgot_password);
        binding.btSubmit.setOnClickListener(v -> {
            hideKeyboard(binding.btSubmit);
            if (!EmailValidator.getInstance().validate(binding.edEmail.getText().toString())) {
                binding.edEmail.setError(getApplication().getString(R.string.error_email));
            }else {
                Toast.makeText(ForgotPasswordActivity.this, "Your password Sent Successfully", Toast.LENGTH_SHORT).show();
            }
        });
        binding.backImage.setOnClickListener(v -> {
            hideKeyboard(binding.backImage);
            finish_activity();
        });
    }
}
