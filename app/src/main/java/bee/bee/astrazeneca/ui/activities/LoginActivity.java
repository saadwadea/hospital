package bee.bee.astrazeneca.ui.activities;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.ActivityLoginBinding;
import bee.bee.astrazeneca.ui.viewmodels.Login;
import bee.bee.astrazeneca.util.EmailValidator;

public class LoginActivity extends BaseActivity {

    Login login;
    ActivityLoginBinding activityLoginBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        login = new ViewModelProvider(this).get(Login.class);
        activityLoginBinding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        activityLoginBinding.setLifecycleOwner(this);
        activityLoginBinding.setLoginViewModel(login);
       // activityLoginBinding.progressbar.setVisibility(View.VISIBLE);
       // hideWindow(activityLoginBinding.progressbar);
        activityLoginBinding.btSignIn.setOnClickListener(v -> {
            //   if (checkInputs()){
            login.onClickSignUp();
            hideKeyboard(activityLoginBinding.btSignIn);
            // }
        });
    }

    private boolean checkInputs() {
        if (!EmailValidator.getInstance().validate(activityLoginBinding.edEmail.getText().toString())) {
            activityLoginBinding.edEmail.setError(getApplication().getString(R.string.error_email));
            return false;
        } else if (activityLoginBinding.edPassword.getText().toString().length() < 6) {
            activityLoginBinding.edPassword.setError(getApplication().getString(R.string.error_password_normal));
            return false;
        }
        return true;
    }
}
