package bee.bee.astrazeneca.ui.viewmodels;

import android.app.ActivityOptions;
import android.app.Application;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.ui.activities.ForgotPasswordActivity;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.activities.RegisterActivity;

public class Register extends AndroidViewModel {

    public Register(@NonNull Application application) {
        super(application);
    }

        public void onClickCreateAccount(){

            Intent intent=new Intent(getApplication(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplication(),R.anim.slide_from_right_to_left,R.anim.slide_in_left);
            getApplication().startActivity(intent,options.toBundle());

        }

}
