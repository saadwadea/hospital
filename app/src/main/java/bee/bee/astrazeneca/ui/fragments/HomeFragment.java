package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import java.util.Timer;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.ApprovalAdapter;
import bee.bee.astrazeneca.adapter.UpcomingEventAdapter;
import bee.bee.astrazeneca.adapter.ViewPagerAdapter;
import bee.bee.astrazeneca.databinding.FragmentHomeBinding;
import bee.bee.astrazeneca.pojo.ApprovalData;
import bee.bee.astrazeneca.pojo.HomeViewPagerData;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.viewmodels.Approval;
import bee.bee.astrazeneca.ui.viewmodels.UpcomingEvent;
import bee.bee.astrazeneca.util.AnimationExpandableList;

public class HomeFragment extends Fragment implements UpcomingEventAdapter.OnItemClickListener {
    MainActivity activity;
    private FragmentHomeBinding homeBinding;
    private UpcomingEvent upcomingEvent;
    private UpcomingEventAdapter upcomingEventAdapter;
    private ApprovalAdapter approvalAdapter;
    private Approval approval;
    private String secondTextOfOne = "Test your knowledge for pre lecture Quiz using the following link:\nPre-Quiz Exam For Pharmacists";
    private String secondTextOfTwo = "Navigate to record videos from command tap at upper part of your browser";
    private String secondTextOfThree = "Select Respiratory /Educational Videos";
    private String secondTextOfFour = "View Event Topic 'Poor Asthma control & SABA over-reliance Speaker Name Dr.Ashraf Al Amir'";
    private String secondTextOfFive = "Test your knowledge & get accredited via finalizing post lecture Quiz using below link:\nPost-Quiz Exam For Pharmacists";
    private int dotscount;
    private ImageView[] dots;
    private ViewPagerAdapter viewPagerAdapter;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            homeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, null, false);
            v = homeBinding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        upcomingEvent = new ViewModelProvider(this).get(UpcomingEvent.class);
        upcomingEventAdapter = new UpcomingEventAdapter(activity, upcomingEvent.getData());
        upcomingEventAdapter.setOnItemClickListener(HomeFragment.this);
        LinearLayoutManager horizontalLayoutManager
                = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        LinearLayoutManager horizontalLayoutManager2
                = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        homeBinding.recyclerView.setLayoutManager(horizontalLayoutManager);
        homeBinding.recyclerView.setAdapter(upcomingEventAdapter);
        approval = new ViewModelProvider(this).get(Approval.class);
        approvalAdapter = new ApprovalAdapter(activity, approval.getData());
        homeBinding.recyclerViewApproval.setLayoutManager(horizontalLayoutManager2);
        homeBinding.recyclerViewApproval.setAdapter(approvalAdapter);
        getData();
        getData2();
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.home));
        activity.hideBackImage();
        activity.showMenu();
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity, R.color.white));
        activity.changeColorMenu(R.drawable.ic_menu_pink_24dp);
        activity.changeColorSearch(R.drawable.icon_search);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity, R.color.grey));
        homeBinding.scrollable.pageScroll(1);
    }

    private void setListener() {
        HomeViewPagerData[]myList=new HomeViewPagerData[]{
                new HomeViewPagerData("Dapagliflozin Local Label Updates by Saudi Food & Drug Authority (SFDA)",R.drawable.photo),
                new HomeViewPagerData("Dapagliflozin Local Label Updates by Saudi Food & Drug Authority (SFDA)",R.drawable.photo),
                new HomeViewPagerData("Dapagliflozin Local Label Updates by Saudi Food & Drug Authority (SFDA)",R.drawable.photo),
                new HomeViewPagerData("Dapagliflozin Local Label Updates by Saudi Food & Drug Authority (SFDA)",R.drawable.photo),
        };
        homeBinding.viewPager.startAutoScroll();
        homeBinding.viewPager.setInterval(7000);
        homeBinding.viewPager.setCycle(true);
        homeBinding.viewPager.setStopScrollWhenTouch(true);
        viewPagerAdapter = new ViewPagerAdapter(activity,myList);
        homeBinding.viewPager.setAdapter(viewPagerAdapter);
        dotscount = viewPagerAdapter.getCount();
        dots = new ImageView[dotscount];

        for (int i = 0; i < dotscount; i++) {

            dots[i] = new ImageView(activity);
            dots[i].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.non_active_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);

            params.setMargins(8, 0, 8, 0);

            homeBinding.SliderDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.active_dot));

        homeBinding.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {

                for (int i = 0; i < dotscount; i++) {
                    dots[i].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.non_active_dot));
                }

                dots[position].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.active_dot));

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        homeBinding.linearExpandableContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                homeBinding.linearExpandable.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                if (homeBinding.linearExpandable.getVisibility()==View.GONE) {

                    AnimationExpandableList.getInstance().expand(homeBinding.linearExpandable);
                    homeBinding.imageIndicator.setImageResource(R.drawable.icon_drop_up);
                } else{

                    AnimationExpandableList.getInstance().collapse(homeBinding.linearExpandable);
                    homeBinding.imageIndicator.setImageResource(R.drawable.icon_drop_down);


                }
            }
        });
        homeBinding.textOne.setText(secondTextOfOne);
        homeBinding.textTwo.setText(secondTextOfTwo);
        homeBinding.textThree.setText(secondTextOfThree);
        homeBinding.textFour.setText(secondTextOfFour);
        homeBinding.textFive.setText(secondTextOfFive);
        homeBinding.cardiovContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new TherapyFragment(), "TherapyFragment", true, true));
        homeBinding.diabetesContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new TherapyFragment(), "TherapyFragment", true, true));
        homeBinding.GastrointestinalContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new TherapyFragment(), "TherapyFragment", true, true));
        homeBinding.RespiratoryContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new TherapyFragment(), "TherapyFragment", true, true));
        homeBinding.oncologyContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new TherapyFragment(), "TherapyFragment", true, true));
        homeBinding.astraZenecaContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new FeedbackFragment(), "FeedbackFragment", true, true));
        homeBinding.exhaleContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new ExhaleFragment(), "ExhaleFragment", true, true));
        homeBinding.RecordedContainer.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new RecordedVideoFragment(), "RecordedVideoFragment", true, true));
    }

    private void getData() {
        upcomingEvent.data.observe(getViewLifecycleOwner(), materialData -> upcomingEventAdapter.setList(materialData));
    }
    private void getData2() {
        approval.data.observe(getViewLifecycleOwner(), approvalData -> approvalAdapter.setList(approvalData));
    }

    @Override
    public void onStart() {
        super.onStart();
        homeBinding.linearExpandable.setVisibility(View.GONE);
        homeBinding.imageIndicator.setImageResource(R.drawable.icon_drop_down);
    }

    @Override
    public void onItemClick(int position) {
        activity.replaceCurrentFragment(R.id.main_fragment,
                new LiveFragment(), "LiveFragment", true, true);
    }
}
