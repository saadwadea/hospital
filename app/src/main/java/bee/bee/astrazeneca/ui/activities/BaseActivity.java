package bee.bee.astrazeneca.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import java.security.MessageDigest;
import bee.bee.astrazeneca.BuildConfig;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.util.Preferences;


public abstract class BaseActivity extends AppCompatActivity {


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences.getInstance().InitializePreferences(this);
        // printHashKey(this);
    }


    static final int callPermissionRequest = 4;
    String Number;

    public void CallMobile(String Number) {
        this.Number = Number;
        try {
            if (Build.VERSION.SDK_INT >= 23 && ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
                    != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{Manifest.permission.CALL_PHONE}, callPermissionRequest);
                return;
            }
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Number));
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                callIntent.setPackage("com.android.server.telecom");
            } else {
                callIntent.setPackage("com.android.phone");
            }
            startActivity(callIntent);
        } catch (Exception e) {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + Number));
            callIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(callIntent);
        }
    }

    public void startNewActivity(String packageName) {
        Intent intent = getApplicationContext().getPackageManager().getLaunchIntentForPackage(packageName);
        if (intent != null) {
            // we found the activity
            // now start the activity
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            // bring user to the market
            // or let them choose an app?
            intent = new Intent(Intent.ACTION_VIEW);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setData(Uri.parse("market://details?id=" + packageName));
            startActivity(intent);
        }
    }

    public void RateApplication() {
        final String appPackageName = getPackageName();
        try {
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id="
                            + appPackageName)));
        } catch (android.content.ActivityNotFoundException anfe) {
            startActivity(new Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id="
                            + appPackageName)));
        }
    }

    void requestReceiveSMSPermission() {
        if (Build.VERSION.SDK_INT >= 23
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.RECEIVE_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            requestPermissions(new String[]{Manifest.permission.RECEIVE_SMS}, 5);
            return;
        }
    }


    public void SetSwipeRefreshLayoutColor(SwipeRefreshLayout swipeRefreshLayout) {
        int colorPrimaryDark = 0;
        colorPrimaryDark = ContextCompat.getColor(this, R.color.colorPrimaryDark);
        swipeRefreshLayout.setColorSchemeColors(colorPrimaryDark, colorPrimaryDark, colorPrimaryDark);
    }

    public void sendSMSTo(String Number, String subject) {
        Uri uri = Uri.parse("smsto:" + Number);
        Intent smsIntent = new Intent(Intent.ACTION_SENDTO, uri);
        smsIntent.putExtra("sms_body", subject);
        startActivity(smsIntent);
    }

    public void finish_activity() {
        finish();
        overridePendingTransition(R.anim.slide_out_left, R.anim.slide_from_left_to_right);
    }

    public void replaceCurrentFragment(int container, Fragment targetFragment, String tag,
                                       boolean addToBackStack, boolean animate) {
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = false;
        fragmentPopped = manager.popBackStackImmediate(tag, 0);
        if (!fragmentPopped && manager.findFragmentByTag(tag) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            if (animate)
                ft.setCustomAnimations(R.anim.push_down_out, R.anim.top_in, R.anim.top_out, R.anim.push_down_in);
            ft.replace(container, targetFragment, tag);
            getSupportFragmentManager().executePendingTransactions();
            if (addToBackStack) {
                ft.addToBackStack(tag);
            }
            ft.commit();
        }
    }

    public void sendMail(String email) {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                "mailto", email, "ContactUsFragment"));
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
        startActivity(Intent.createChooser(emailIntent, "Send email..."));
    }

    public void emptyText(TextView textView, String value) {
        if (value.equals("null")) {
            textView.setText("");
        } else {
            textView.setText(value);
        }
    }

    public void emptyEditText(EditText editText, String value) {
        if (value.equals("null")) {
            editText.setText("");
        } else {
            editText.setText(value);
        }
    }

    public void visibilityImage(String image_url, ImageView imageViewDisabled, ImageView imageViewEnabled) {
        if (image_url.equals("null") || image_url.equals("")) {
            imageViewDisabled.setVisibility(View.VISIBLE);
            imageViewEnabled.setVisibility(View.GONE);
        } else {
            imageViewDisabled.setVisibility(View.GONE);
            imageViewEnabled.setVisibility(View.VISIBLE);
        }
    }

    public void openSettings() {
        Intent intent = new Intent();
        intent.setAction(
                Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("bee.bee.liveapp",
                BuildConfig.APPLICATION_ID, null);
        intent.setData(uri);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void printHashKey(Context pContext) {
        try {
            @SuppressLint("PackageManagerGetSignatures") PackageInfo info = getPackageManager().getPackageInfo(getPackageName(), PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                String hashKey = new String(Base64.encode(md.digest(), 0));
                Log.i("hashKey", "printHashKey() Hash Key: " + hashKey);
            }
        } catch (Exception e) {
            Log.e("", "printHashKey()", e);
        }
    }

    public void hideWindow(ProgressBar progressBar) {
        progressBar.setVisibility(View.VISIBLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void showWindow(ProgressBar progressBar) {
        progressBar.setVisibility(View.GONE);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getApplicationWindowToken(), 0);
    }

    public void showKeyboard() {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }

    /*  public void expand(View layout, int layoutHeight) {
          layout.setVisibility(View.VISIBLE);
          ValueAnimator animator = slideAnimator(layout, 0, layoutHeight);
          animator.start();
      }

      public void collapse(final View layout) {
          int finalHeight = layout.getHeight();
          ValueAnimator mAnimator = slideAnimator(layout, finalHeight, 0);

          mAnimator.addListener(new Animator.AnimatorListener() {
              @Override
              public void onAnimationEnd(Animator animator) {
                  //Height=0, but it set visibility to GONE
                  layout.setVisibility(View.GONE);
              }

              @Override
              public void onAnimationStart(Animator animator) {
              }

              @Override
              public void onAnimationCancel(Animator animator) {
              }

              @Override
              public void onAnimationRepeat(Animator animator) {
              }
          });
          mAnimator.start();
      }*/
   /* public ValueAnimator slideAnimator(final View layout, int start, int end) {
        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
                layoutParams.height = value;
                layout.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }*/
    public void expand(final View v, final TextView textExpandColor, final ImageView imageExpandResource, final View viewToChangeBackground) {
        textExpandColor.setTextColor(ContextCompat.getColor(this, R.color.health_button_color));
        imageExpandResource.setImageResource(R.drawable.ic_keyboard_arrow_up_24dp);
        viewToChangeBackground.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_terms_views));
        int matchParentMeasureSpec = View.MeasureSpec.makeMeasureSpec(((View) v.getParent()).getWidth(), View.MeasureSpec.EXACTLY);
        int wrapContentMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        v.measure(matchParentMeasureSpec, wrapContentMeasureSpec);
        final int targetHeight = v.getMeasuredHeight();

        // Older versions of android (pre API 21) cancel animations for views with a height of 0.
        v.getLayoutParams().height = 1;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int) (targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Expansion speed of 1dp/ms
        a.setDuration((int) (targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public void collapse(final View v, final TextView textCollapseColor, final ImageView imageCollapseResource, final View viewToChangeBackground) {
        textCollapseColor.setTextColor(ContextCompat.getColor(this, R.color.white));
        imageCollapseResource.setImageResource(R.drawable.icons_chevron);
        viewToChangeBackground.setBackground(ContextCompat.getDrawable(this, R.drawable.rounded_terms_views_point));
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation() {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if (interpolatedTime == 1) {

                    v.setVisibility(View.GONE);
                } else {
                    v.getLayoutParams().height = initialHeight - (int) (initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // Collapse speed of 1dp/ms
        a.setDuration((int) (initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
    public boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}