package bee.bee.astrazeneca.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;

public class UpcomingEvent extends ViewModel {
   public MutableLiveData<MaterialData[]>data=new MutableLiveData<>();

    public MaterialData[] getData() {
        MaterialData[] myListData = new MaterialData[] {
                new MaterialData(R.drawable.image_event,"1st GCC Respiratory Submit","16 October 2019"),
                new MaterialData(R.drawable.image_event,"1st GCC Respiratory Submit","16 October 2019"),
                new MaterialData(R.drawable.image_event,"1st GCC Respiratory Submit","16 October 2019"),
                new MaterialData(R.drawable.image_event,"1st GCC Respiratory Submit","16 October 2019"),
                new MaterialData(R.drawable.image_event,"1st GCC Respiratory Submit","16 October 2019"),

        };
        data.setValue(myListData);
        return myListData;
    }
}