package bee.bee.astrazeneca.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.widget.MediaController;
import android.widget.Toast;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.ActivityDetailsBinding;
import bee.bee.astrazeneca.ui.fragments.RecordedVideoDetailsFragment;
import bee.bee.astrazeneca.util.FullScreenMediaController;
import bg.devlabs.fullscreenvideoview.listener.mediacontroller.MediaControllerListener;

public class DetailsActivity extends BaseActivity {
    ActivityDetailsBinding binding;
    private MediaController mediaController;
    private static final String VIDEO_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding= DataBindingUtil.setContentView(this,R.layout.activity_details);
        String fullScreen =  getIntent().getStringExtra("fullScreenInd");
        if("y".equals(fullScreen)){
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                    WindowManager.LayoutParams.FLAG_FULLSCREEN);
            getSupportActionBar().hide();
        }

        binding.videoView.setVideoPath(VIDEO_URL);

        mediaController = new FullScreenMediaController(this);
        mediaController.setAnchorView(binding.videoView);

        binding.videoView.setMediaController(mediaController);
        binding.videoView.start();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
