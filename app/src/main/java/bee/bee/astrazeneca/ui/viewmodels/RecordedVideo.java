package bee.bee.astrazeneca.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.pojo.RecordedVideoData;

public class RecordedVideo extends ViewModel {
   public MutableLiveData<RecordedVideoData[]>data=new MutableLiveData<>();

    public RecordedVideoData[] getData() {
        RecordedVideoData[] myListData = new RecordedVideoData[] {
                new RecordedVideoData(R.drawable.video,"T2DM & Heart failure DO we need more investigation?","Dr.Said Khader FRCP UK"),
                new RecordedVideoData(R.drawable.video,"T2DM & Heart failure DO we need more investigation?","Dr.Said Khader FRCP UK"),
                new RecordedVideoData(R.drawable.video,"T2DM & Heart failure DO we need more investigation?","Dr.Said Khader FRCP UK"),
                new RecordedVideoData(R.drawable.video,"T2DM & Heart failure DO we need more investigation?","Dr.Said Khader FRCP UK"),

        };
        data.setValue(myListData);
        return myListData;
    }
}