package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentContactusBinding;
import bee.bee.astrazeneca.databinding.FragmentSearchBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;


public class SearchFragment extends Fragment {
    MainActivity activity;
    private FragmentSearchBinding binding;
    View v;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.Search));
        activity.showBackImage();
        activity.hideMenu();
        activity.hideSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorBack(R.drawable.ic_arrow_back_white_24dp);
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
    }

    private void setListener() {
        binding.imageSearch.setOnClickListener(v -> {
            activity.hideKeyboard(binding.imageSearch);
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new SearchResultFragment(), "SearchResultFragment", true, true);
        });
        binding.searchContainer.setOnClickListener(v -> {
            activity.hideKeyboard(binding.searchContainer);
            activity.replaceCurrentFragment(R.id.main_fragment,
                    new SearchResultFragment(), "SearchResultFragment", true, true);
        });
    }

}
