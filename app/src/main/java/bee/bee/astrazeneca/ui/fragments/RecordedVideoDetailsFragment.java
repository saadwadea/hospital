package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentRecordedVideoDetailsBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.util.AnimationExpandableList;
import tcking.github.com.giraffeplayer2.VideoInfo;

public class RecordedVideoDetailsFragment extends Fragment {
    MainActivity activity;
    private int aspectRatio = VideoInfo.AR_ASPECT_FIT_PARENT;
    FragmentRecordedVideoDetailsBinding binding;
    private static final String VIDEO_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
    View v;

    private int mResumeWindow;
    private long mResumePosition;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recorded_video_details, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.Recorded_Videos));
        activity.hideMenu();
        activity.showBackImage();
        activity.showSearchImage();
        activity.changeColorBack(R.drawable.ic_arrow_back_white_24dp);
        activity.changeColorToolbar(ContextCompat.getColor(activity, R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity, R.color.white));
    }

    private void setListener() {

        binding.playerView.setVideoPath(VIDEO_URL);
        binding.playerView.getPlayer().start();
        /* binding.videoView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.youtube.com/embed/AN2p3PIpGJA"));
                intent.putExtra("force_fullscreen",true);
                startActivity(intent);
            }
        });*/

        binding.diabetesContainer.setOnClickListener(v -> {
            // binding.expandableContainerDiabetes.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (binding.expandableContainerDiabetes.getVisibility() == View.GONE) {
                AnimationExpandableList.getInstance().expand(binding.expandableContainerDiabetes);
                binding.imageDiabetes.setImageResource(R.drawable.icons_cheveron_up);
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerEducational);
                binding.imageEducational.setImageResource(R.drawable.icons_chevron);
            } else {
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerDiabetes);
                binding.imageDiabetes.setImageResource(R.drawable.icons_chevron);

            }
        });
        binding.educationalContainer.setOnClickListener(v -> {
            // binding.expandableContainerEducational.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (binding.expandableContainerEducational.getVisibility() == View.GONE) {
                AnimationExpandableList.getInstance().expand(binding.expandableContainerEducational);
                binding.imageEducational.setImageResource(R.drawable.icons_cheveron_up);
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerDiabetes);
                binding.imageDiabetes.setImageResource(R.drawable.icons_chevron);
            } else {
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerEducational);
                binding.imageEducational.setImageResource(R.drawable.icons_chevron);

            }
        });
    }

}
