package bee.bee.astrazeneca.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;

import com.blogspot.atifsoftwares.animatoolib.Animatoo;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.ActivitySplashBinding;
import bee.bee.astrazeneca.ui.viewmodels.Splash;

public class SplashActivity extends BaseActivity {
    Splash splash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //  overridePendingTransition(R.anim.fade_out,R.anim.fade_in);
        ActivitySplashBinding activitySplashBinding = DataBindingUtil.setContentView(this, R.layout.activity_splash);
        splash = new ViewModelProvider(this).get(Splash.class);
        activitySplashBinding.setSplash(splash);
        activitySplashBinding.setLifecycleOwner(this);

    }
}
