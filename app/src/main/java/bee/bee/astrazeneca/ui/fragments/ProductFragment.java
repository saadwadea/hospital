package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentMainBinding;
import bee.bee.astrazeneca.databinding.FragmentProductBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;


public class ProductFragment extends Fragment {
    MainActivity activity;
    private FragmentProductBinding binding;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_product, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.BRILINTA));
        activity.showBackImage();
        activity.hideMenu();
        activity.changeColorBack(R.drawable.ic_arrow_back_white_24dp);
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
    }

    private void setListener() {
        binding.indicationsContainer.setOnClickListener(v -> {

            if (binding.textDetailsIndications.getVisibility()==View.VISIBLE)
            {
                activity.collapse(binding.textDetailsIndications,binding.textPoint,binding.imageIndications,binding.indicationsContainer);
            }
            else
            {
                activity.expand(binding.textDetailsIndications,binding.textPoint,binding.imageIndications,binding.indicationsContainer);
                activity.collapse(binding.textDetailsMedicine,binding.textPoint2,binding.imageMedicine,binding.MedicineContainer);
                activity.collapse(binding.textDetailsActions,binding.textPoint3,binding.imageAction,binding.actionContainer);
                activity.collapse(binding.textDetailsAdmin,binding.textPoint4,binding.imageAdmin,binding.administratorContainer);

            }
        });
        binding.MedicineContainer.setOnClickListener(v -> {
            if (binding.textDetailsMedicine.getVisibility()==View.GONE)
            {
                activity.expand(binding.textDetailsMedicine,binding.textPoint2,binding.imageMedicine,binding.MedicineContainer);
                activity.collapse(binding.textDetailsIndications,binding.textPoint,binding.imageIndications,binding.indicationsContainer);
                activity.collapse(binding.textDetailsActions,binding.textPoint3,binding.imageAction,binding.actionContainer);
                activity.collapse(binding.textDetailsAdmin,binding.textPoint4,binding.imageAdmin,binding.administratorContainer);

            }
            else
            {
                activity.collapse(binding.textDetailsMedicine,binding.textPoint2,binding.imageMedicine,binding.MedicineContainer);

            }
        });
        binding.actionContainer.setOnClickListener(v -> {
            if (binding.textDetailsActions.getVisibility()==View.GONE)
            {
                activity.expand(binding.textDetailsActions,binding.textPoint3,binding.imageAction,binding.actionContainer);
                activity.collapse(binding.textDetailsIndications,binding.textPoint,binding.imageIndications,binding.indicationsContainer);
                activity.collapse(binding.textDetailsMedicine,binding.textPoint2,binding.imageMedicine,binding.MedicineContainer);
                activity.collapse(binding.textDetailsAdmin,binding.textPoint4,binding.imageAdmin,binding.administratorContainer);

            }
            else
            {
                activity.collapse(binding.textDetailsActions,binding.textPoint3,binding.imageAction,binding.actionContainer);

            }
        });
        binding.administratorContainer.setOnClickListener(v -> {
            if (binding.textDetailsAdmin.getVisibility()==View.GONE)
            {
                activity.expand(binding.textDetailsAdmin,binding.textPoint4,binding.imageAdmin,binding.administratorContainer);
                activity.collapse(binding.textDetailsIndications,binding.textPoint,binding.imageIndications,binding.indicationsContainer);
                activity.collapse(binding.textDetailsMedicine,binding.textPoint2,binding.imageMedicine,binding.MedicineContainer);
                activity.collapse(binding.textDetailsActions,binding.textPoint3,binding.imageAction,binding.actionContainer);

            }
            else
            {
                activity.collapse(binding.textDetailsAdmin,binding.textPoint4,binding.imageAdmin,binding.administratorContainer);

            }
        });
    }

}
