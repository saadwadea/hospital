package bee.bee.astrazeneca.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.pojo.SearchData;

public class SearchResult extends ViewModel {
   public MutableLiveData<SearchData[]>data=new MutableLiveData<>();

    public SearchData[] getData() {
        SearchData[] myListData = new SearchData[] {
                new SearchData("Diabetes Mellitus-Module 1 handout","16 April 2019","Diabetes","Forxige(dapaglifozin"),
                new SearchData("Diabetes Mellitus-Module 1 handout","16 April 2019","Diabetes","Forxige(dapaglifozin"),
                new SearchData("Diabetes Mellitus-Module 1 handout","16 April 2019","Diabetes","Forxige(dapaglifozin"),
                new SearchData("Diabetes Mellitus-Module 1 handout","16 April 2019","Diabetes","Forxige(dapaglifozin"),
                new SearchData("Diabetes Mellitus-Module 1 handout","16 April 2019","Diabetes","Forxige(dapaglifozin"),

        };
        data.setValue(myListData);
        return myListData;
    }
}