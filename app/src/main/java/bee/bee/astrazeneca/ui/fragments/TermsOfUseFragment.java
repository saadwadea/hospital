package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentLiveBinding;
import bee.bee.astrazeneca.databinding.FragmentTermsUseBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;


public class TermsOfUseFragment extends Fragment {
    MainActivity activity;
    FragmentTermsUseBinding binding;
    View v;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_terms_use, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.terms_of_use));
        activity.hideBackImage();
        activity.showMenu();
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
    }

    private void setListener() {
        binding.pointOneContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.textDetailsOne.getVisibility()==View.VISIBLE)
                {
                    activity.collapse(binding.textDetailsOne,binding.textPoint,binding.imageOne,binding.pointOneContainer);

                }
                else
                {
                    activity.expand(binding.textDetailsOne,binding.textPoint,binding.imageOne,binding.pointOneContainer);
                    activity.collapse(binding.textDetailsTwo,binding.textPoint2,binding.imageTwo,binding.secondPointContainer);
                    activity.collapse(binding.textDetailsThree,binding.textPoint3,binding.imageThree,binding.thirdContainer);
                    activity.collapse(binding.textDetailsFour,binding.textPoint4,binding.imageFour,binding.fourthContainer);
                    activity.collapse(binding.textFivePoint,binding.textPoint5,binding.imageFive,binding.fiveContainer);


                }
            }
        });
        binding.secondPointContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.textDetailsTwo.getVisibility()==View.GONE)
                {
                    activity.expand(binding.textDetailsTwo,binding.textPoint2,binding.imageTwo,binding.secondPointContainer);

                    activity.collapse(binding.textDetailsOne,binding.textPoint,binding.imageOne,binding.pointOneContainer);
                    activity.collapse(binding.textDetailsThree,binding.textPoint3,binding.imageThree,binding.thirdContainer);
                    activity.collapse(binding.textDetailsFour,binding.textPoint4,binding.imageFour,binding.fourthContainer);
                    activity.collapse(binding.textFivePoint,binding.textPoint5,binding.imageFive,binding.fiveContainer);
                }
                else
                {
                    activity.collapse(binding.textDetailsTwo,binding.textPoint2,binding.imageTwo,binding.secondPointContainer);


                }
            }
        });
        binding.thirdContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.textDetailsThree.getVisibility()==View.GONE)
                {
                    activity.expand(binding.textDetailsThree,binding.textPoint3,binding.imageThree,binding.thirdContainer);
                    activity.collapse(binding.textDetailsOne,binding.textPoint,binding.imageOne,binding.pointOneContainer);
                    activity.collapse(binding.textDetailsTwo,binding.textPoint2,binding.imageTwo,binding.secondPointContainer);
                    activity.collapse(binding.textDetailsFour,binding.textPoint4,binding.imageFour,binding.fourthContainer);
                    activity.collapse(binding.textFivePoint,binding.textPoint5,binding.imageFive,binding.fiveContainer);
                }
                else
                {
                    activity.collapse(binding.textDetailsThree,binding.textPoint3,binding.imageThree,binding.thirdContainer);

                }
            }
        });
        binding.fourthContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.textDetailsFour.getVisibility()==View.GONE)
                {
                    activity.expand(binding.textDetailsFour,binding.textPoint4,binding.imageFour,binding.fourthContainer);
                    activity.collapse(binding.textDetailsOne,binding.textPoint,binding.imageOne,binding.pointOneContainer);
                    activity.collapse(binding.textDetailsThree,binding.textPoint3,binding.imageThree,binding.thirdContainer);
                    activity.collapse(binding.textDetailsTwo,binding.textPoint2,binding.imageTwo,binding.secondPointContainer);
                    activity.collapse(binding.textFivePoint,binding.textPoint5,binding.imageFive,binding.fiveContainer);
                }
                else
                {
                    activity.collapse(binding.textDetailsFour,binding.textPoint4,binding.imageFour,binding.fourthContainer);


                }
            }
        });
        binding.fiveContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (binding.textFivePoint.getVisibility()==View.GONE)
                {
                    activity.expand(binding.textFivePoint,binding.textPoint5,binding.imageFive,binding.fiveContainer);
                    activity.collapse(binding.textDetailsOne,binding.textPoint,binding.imageOne,binding.pointOneContainer);
                    activity.collapse(binding.textDetailsThree,binding.textPoint3,binding.imageThree,binding.thirdContainer);
                    activity.collapse(binding.textDetailsFour,binding.textPoint4,binding.imageFour,binding.fourthContainer);
                    activity.collapse(binding.textDetailsTwo,binding.textPoint2,binding.imageTwo,binding.secondPointContainer);
                }
                else
                {
                    activity.collapse(binding.textFivePoint,binding.textPoint5,binding.imageFive,binding.fiveContainer);

                }
            }
        });
    }

}
