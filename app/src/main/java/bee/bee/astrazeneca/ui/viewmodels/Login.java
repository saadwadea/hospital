package bee.bee.astrazeneca.ui.viewmodels;

import android.app.ActivityOptions;
import android.app.Application;
import android.content.Intent;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.LoginUser;
import bee.bee.astrazeneca.ui.activities.ForgotPasswordActivity;
import bee.bee.astrazeneca.ui.activities.LoginActivity;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.activities.RegisterActivity;
import bee.bee.astrazeneca.util.EmailValidator;

public class Login extends AndroidViewModel {

    public Login(@NonNull Application application) {
        super(application);
    }

        public void onClickSignUp (){

            Intent intent=new Intent(getApplication(), MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplication(),R.anim.slide_from_right_to_left,R.anim.slide_in_left);
            getApplication().startActivity(intent,options.toBundle());

        }
       public void onClickForgotPassword (){

        Intent intent=new Intent(getApplication(), ForgotPasswordActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplication(),R.anim.slide_from_right_to_left,R.anim.slide_in_left);
        getApplication().startActivity(intent,options.toBundle());

    }
    public void onClickRegister(){

        Intent intent=new Intent(getApplication(), RegisterActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplication(),R.anim.slide_from_right_to_left,R.anim.slide_in_left);
        getApplication().startActivity(intent,options.toBundle());

    }


}
