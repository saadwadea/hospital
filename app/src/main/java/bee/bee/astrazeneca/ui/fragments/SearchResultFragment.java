package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.SearchResultAdapter;
import bee.bee.astrazeneca.adapter.UsefulMaterialAdapter;
import bee.bee.astrazeneca.databinding.FragmentResultSearchBinding;
import bee.bee.astrazeneca.databinding.FragmentSearchBinding;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.pojo.SearchData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.viewmodels.SearchResult;


public class SearchResultFragment extends Fragment {
    MainActivity activity;
    private FragmentResultSearchBinding binding;
    private SearchResult result;
    SearchResultAdapter searchResultAdapter;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_result_search, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        result= new ViewModelProvider(this).get(SearchResult.class);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.Search_Result));
        activity.hideBackImage();
        activity.showMenu();
        activity.hideSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity, R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity, R.color.white));
    }

    private void setListener() {
        getData();

    }

    private void getData() {
        searchResultAdapter = new SearchResultAdapter(activity, result.getData());
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerView.setAdapter(searchResultAdapter);
        result.data.observe(getViewLifecycleOwner(), data -> searchResultAdapter.setList(data));

    }

}
