package bee.bee.astrazeneca.ui.viewmodels;

import android.app.ActivityOptions;
import android.app.Application;
import android.content.Context;
import android.content.Intent;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.ViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.ui.activities.LoginActivity;

public class Splash extends AndroidViewModel {

    public Splash(@NonNull Application application) {
        super(application);
    }

    public void signIn(){
        Intent intent=new Intent(getApplication(), LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplication(),R.anim.slide_from_right_to_left,R.anim.slide_in_left);
        getApplication().startActivity(intent,options.toBundle());
    }
}
