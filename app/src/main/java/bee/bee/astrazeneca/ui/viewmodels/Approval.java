package bee.bee.astrazeneca.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.List;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.ApprovalData;
import bee.bee.astrazeneca.pojo.MaterialData;
import io.reactivex.rxjava3.annotations.NonNull;
import io.reactivex.rxjava3.core.Observable;
import io.reactivex.rxjava3.core.Observer;

public class Approval extends ViewModel {
   public MutableLiveData<ApprovalData[]>data=new MutableLiveData<>();

    public ApprovalData[] getData() {
        ApprovalData[] myListData = new ApprovalData[] {
                new ApprovalData(R.drawable.gcc,"SFDA approved Dapagliflozin label update as regards to patients with renal impaiment"),
                new ApprovalData(R.drawable.tagrisso,"SFDA has approved Tagrisso as 1st-line trearment for EFGRm NSCLC based on positive"),
                new ApprovalData(R.drawable.tagrisso,"SFDA has approved Tagrisso as 1st-line trearment for EFGRm NSCLC based on positive"),
                new ApprovalData(R.drawable.tagrisso,"SFDA has approved Tagrisso as 1st-line trearment for EFGRm NSCLC based on positive"),
                new ApprovalData(R.drawable.tagrisso,"SFDA has approved Tagrisso as 1st-line trearment for EFGRm NSCLC based on positive"),

        };
        data.setValue(myListData);
        return myListData;
    }
}