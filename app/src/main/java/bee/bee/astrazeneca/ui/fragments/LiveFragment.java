package bee.bee.astrazeneca.ui.fragments;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.URLUtil;
import android.widget.MediaController;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.universalvideoview.UniversalVideoView;

import java.io.IOException;
import java.util.ArrayList;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.UsefulMaterialAdapter;
import bee.bee.astrazeneca.databinding.FragmentLiveBinding;
import bee.bee.astrazeneca.databinding.FragmentUsefulMaterialBinding;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.viewmodels.UsefulMaterial;
import bg.devlabs.fullscreenvideoview.listener.mediacontroller.MediaControllerListener;

import static android.content.ContentValues.TAG;


public class LiveFragment extends Fragment {
    MainActivity activity;
    FragmentLiveBinding binding;
    private static final String VIDEO_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_live, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();

    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.liveEvents));
        activity.hideBackImage();
        activity.showMenu();
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity, R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity, R.color.white));
    }

    private void setListener() {
        binding.videoView.setVideoPath(VIDEO_URL);
        binding.videoView.getPlayer().start();
    }


}
