package bee.bee.astrazeneca.ui.activities;

import android.os.Bundle;
import android.view.View;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.ActivityMainBinding;
import bee.bee.astrazeneca.ui.fragments.HomeFragment;
import bee.bee.astrazeneca.ui.fragments.MenuFragment;
import bee.bee.astrazeneca.ui.fragments.ProfileFragment;
import bee.bee.astrazeneca.ui.fragments.SearchFragment;

public class MainActivity extends BaseActivity {
    private static final String TAG = "MainActivity";
    ActivityMainBinding activityMainBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        initializeSlideMenu();

        replaceCurrentFragment(R.id.main_fragment,
                new HomeFragment(), "HomeFragment",
                true, false);


    }


    void initializeSlideMenu() {
        // slide menu def
        activityMainBinding.imgViewMenuMainActivity.setOnClickListener(view -> openMenu());
        activityMainBinding.imgViewBack.setOnClickListener(v -> {
            hideKeyboard(activityMainBinding.imgViewBack);
            FragmentManager manager = getSupportFragmentManager();
            if (manager.getBackStackEntryCount() > 0) {
                manager.popBackStack();
            } else {
                onBackPressed();
            }
        });
        activityMainBinding.imgSearch.setOnClickListener(v -> replaceCurrentFragment(R.id.main_fragment,
                new SearchFragment(), "SearchFragment",
                true, true));

        if (getSupportFragmentManager().findFragmentByTag("MenuFragment") == null)
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.menu, MenuFragment.getInstance(), "MenuFragment")
                    .commit();
        setDrawerState(true);
    }

    public void setDrawerState(boolean isEnabled) {
        if (isEnabled) {
            activityMainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            activityMainBinding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    public void closeMenu() {
        activityMainBinding.drawerLayout.closeDrawer(GravityCompat.START);
    }

    public void openMenu() {
        activityMainBinding.drawerLayout.openDrawer(GravityCompat.START);
    }

    public void hideMenu() {
        activityMainBinding.imgViewMenuMainActivity.setVisibility(View.GONE);
    }

    public void showMenu() {
        activityMainBinding.imgViewMenuMainActivity.setVisibility(View.VISIBLE);
    }

    public void hideToolbar() {
        activityMainBinding.toolbar.setVisibility(View.GONE);

    }

    public void showToolbar() {
        activityMainBinding.toolbar.setVisibility(View.VISIBLE);

    }

    public void changeColorToolbar(int color) {
        activityMainBinding.toolbar.setBackgroundColor(color);
    }

    public void changeColorMenu(int drawable) {
        activityMainBinding.imgViewMenuMainActivity.setImageResource(drawable);
    }

    public void changeColorSearch(int drawable) {
        activityMainBinding.imgSearch.setImageResource(drawable);
    }
    public void changeColorBack(int drawable) {
        activityMainBinding.imgViewBack.setImageResource(drawable);
    }

    public void showSearchImage() {
        activityMainBinding.imgSearch.setVisibility(View.VISIBLE);
    }

    public void hideSearchImage() {
        activityMainBinding.imgSearch.setVisibility(View.GONE);
    }


    public void hideBackImage() {
        activityMainBinding.imgViewBack.setVisibility(View.GONE);
    }

    public void showBackImage() {
        activityMainBinding.imgViewBack.setVisibility(View.VISIBLE);
    }

    public void setHeaderTitle(String title) {
        activityMainBinding.txtviewTitleMainActivity.setText(title);
    }

    public void changeColorHeaderTitle(int color) {
        activityMainBinding.txtviewTitleMainActivity.setTextColor(color);
    }


    public void replaceCurrentFragment(int container, Fragment targetFragment, String tag,
                                       boolean addToBackStack, boolean animate) {
        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = false;
        fragmentPopped = manager.popBackStackImmediate(tag, 0);
        if (!fragmentPopped && manager.findFragmentByTag(tag) == null) {
            FragmentTransaction ft = manager.beginTransaction();
            if (animate)
                ft.setCustomAnimations(R.anim.slide_from_right_to_left, R.anim.slide_in_left,
                        R.anim.slide_out_left, R.anim.slide_from_left_to_right);
            //       ft.setCustomAnimations(R.anim.push_down_out, R.anim.top_in, R.anim.top_out, R.anim.push_down_in);
            ft.replace(container, targetFragment, tag);
            getSupportFragmentManager().executePendingTransactions();
            if (addToBackStack) {
                ft.addToBackStack(tag);
            }
            ft.commit();
        }
    }

    public String getCurrentFragment() {
        // reset selected icons
        return getSupportFragmentManager().getBackStackEntryAt(
                getSupportFragmentManager().getBackStackEntryCount() - 1)
                .getName();
    }

    @Override
    public void onBackPressed() {
        if (activityMainBinding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            closeMenu();
        } else {
            // If the fragment exists and has some back-stack entry
            String fragmentTag = getSupportFragmentManager().getBackStackEntryAt(
                    getSupportFragmentManager().getBackStackEntryCount() - 1)
                    .getName();
            if (fragmentTag != null && fragmentTag.equals("ProfileFragment")) {
                ProfileFragment fragment = (ProfileFragment) getSupportFragmentManager()
                        .findFragmentByTag("ProfileFragment");
                if (fragment != null && fragment.getChildFragmentManager().getBackStackEntryCount() > 1) {
                    //this code to back from tabs profile fragment
                    fragment.getChildFragmentManager().popBackStack();
                    // reset selected tabs
                } else {
                    super.onBackPressed();
                    // reset selected icons
                    getCurrentFragment();
                }
            } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
                finish_activity();
            } else {
                super.onBackPressed();
                // reset selected icons
                getCurrentFragment();
            }
        }
    }
    public void backPress(){
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() > 0) {
            manager.popBackStack();
        } else {
            onBackPressed();
        }
    }

}
