package bee.bee.astrazeneca.ui.fragments;


import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.FragmentChangePasswordBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;

public class ChangePasswordFragment extends Fragment {

    FragmentChangePasswordBinding binding;
    MainActivity activity;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }
    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.change_password));
        activity.hideMenu();
        activity.showBackImage();
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorBack(R.drawable.ic_arrow_back_white_24dp);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
    }
    private void setListener(){
        binding.btSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.backPress();
            }
        });

    }
    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }
}
