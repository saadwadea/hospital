package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import com.google.android.material.tabs.TabLayout;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.ViewPagerBrilintaAdapter;
import bee.bee.astrazeneca.databinding.FragmentBrilintaBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;


public class BrilintaFragment extends Fragment {
    MainActivity activity;
    private FragmentBrilintaBinding binding;
    ViewPagerBrilintaAdapter adapter;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_brilinta, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.BRILINTA));
        activity.showBackImage();
        activity.hideMenu();
        activity.changeColorBack(R.drawable.ic_arrow_back_white_24dp);
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
    }

    private void setListener() {
        adapter=new ViewPagerBrilintaAdapter(getChildFragmentManager(),binding.tabs.getTabCount());
        binding.viewpager.setAdapter(adapter);
        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                binding.viewpager.setCurrentItem(tab.getPosition());

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        binding.viewpager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(binding.tabs));
    }


}
