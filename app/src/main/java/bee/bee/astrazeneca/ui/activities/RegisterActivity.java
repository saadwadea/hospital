package bee.bee.astrazeneca.ui.activities;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.ViewModelProvider;
import android.os.Bundle;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.databinding.ActivityRegisterBinding;
import bee.bee.astrazeneca.ui.viewmodels.Register;
import bee.bee.astrazeneca.util.EmailValidator;

public class RegisterActivity extends BaseActivity {
    Register register;
    ActivityRegisterBinding activityRegisterBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        register= new ViewModelProvider(this).get(Register.class);
        activityRegisterBinding = DataBindingUtil.setContentView(this, R.layout.activity_register);
        activityRegisterBinding.backImage.setOnClickListener(v -> {
            hideKeyboard(activityRegisterBinding.backImage);
            finish_activity();
        });
        activityRegisterBinding.btSubmit.setOnClickListener(v -> register.onClickCreateAccount());

    }

    private boolean checkInputs() {
        if (activityRegisterBinding.edFirstName.getText().toString().length()< 2 ){
            activityRegisterBinding.edFirstName.setError(getString(R.string.error_name));
            return false;
        }
        else if (activityRegisterBinding.edLastName.getText().toString().length()< 2){
            activityRegisterBinding.edLastName.setError(getString(R.string.error_name));
            return false;
        }
        else if (activityRegisterBinding.edMobile.getText().toString().length()<11){
            activityRegisterBinding.edMobile.setError(getString(R.string.error_number));
            return false;
        }
        else if (!EmailValidator.getInstance().validate(activityRegisterBinding.edEmail.getText().toString())) {
            activityRegisterBinding.edEmail.setError(getApplication().getString(R.string.error_email));
            return false;
        } else if (activityRegisterBinding.edPassword.getText().toString().length() < 6) {
            activityRegisterBinding.edPassword.setError(getApplication().getString(R.string.error_password_normal));
            return false;
        } else if (!activityRegisterBinding.edConfirmPassword.getText().toString().equals(activityRegisterBinding.edPassword.getText().toString())) {
            activityRegisterBinding.edConfirmPassword.setError(getString(R.string.error_matching));
            return false;
        }

        return true;
    }
}
