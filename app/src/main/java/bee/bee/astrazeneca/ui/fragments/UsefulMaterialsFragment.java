package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.viewpager.widget.ViewPager;

import java.util.ArrayList;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.UsefulMaterialAdapter;
import bee.bee.astrazeneca.adapter.ViewPagerAdapter;
import bee.bee.astrazeneca.databinding.FragmentHomeBinding;
import bee.bee.astrazeneca.databinding.FragmentUsefulMaterialBinding;
import bee.bee.astrazeneca.pojo.MaterialData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.viewmodels.UsefulMaterial;


public class UsefulMaterialsFragment extends Fragment implements UsefulMaterialAdapter.OnItemClickListener {
    MainActivity activity;
    FragmentUsefulMaterialBinding usefulMaterialBinding;
    UsefulMaterial usefulMaterialViewModel;
    UsefulMaterialAdapter adapter;
    View v;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            usefulMaterialBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_useful_material, null, false);
            v = usefulMaterialBinding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        usefulMaterialViewModel= new ViewModelProvider(this).get(UsefulMaterial.class);
        setListener();
        adapter.setOnItemClickListener(UsefulMaterialsFragment.this);

    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.useful_materials));
        activity.hideBackImage();
        activity.showMenu();
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity,R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity,R.color.white));
    }

    private void setListener() {
        getData();
        usefulMaterialBinding.btHealth.setBackgroundResource(0);
        usefulMaterialBinding.btPatients.setTextColor(Color.WHITE);
        usefulMaterialBinding.btPatients.setBackgroundResource(R.drawable.rounded_patient_button);
        usefulMaterialBinding.btHealth.setTextColor(Color.parseColor("#454F63"));
        usefulMaterialBinding.btHealth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefulMaterialBinding.btHealth.setBackgroundResource(0);
                usefulMaterialBinding.btPatients.setTextColor(Color.WHITE);
                usefulMaterialBinding.btPatients.setBackgroundResource(R.drawable.rounded_patient_button);
                usefulMaterialBinding.btHealth.setTextColor(Color.parseColor("#454F63"));

                getData();

            }
        });
        usefulMaterialBinding.btPatients.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                usefulMaterialBinding.btHealth.setBackgroundResource(R.drawable.rounded_health_button);
                usefulMaterialBinding.btHealth.setTextColor(Color.WHITE);
                usefulMaterialBinding.btPatients.setBackgroundResource(0);
                usefulMaterialBinding.btPatients.setTextColor(Color.parseColor("#454F63"));

                getData2();
            }
        });

    }
private void getData(){
    adapter=new UsefulMaterialAdapter(activity,usefulMaterialViewModel.getData());
    usefulMaterialBinding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
    usefulMaterialBinding.recyclerView.setAdapter(adapter);
  usefulMaterialViewModel.data.observe(getViewLifecycleOwner(), materialData -> adapter.setList(materialData));

}
    private void getData2(){
        adapter=new UsefulMaterialAdapter(activity,usefulMaterialViewModel.getData2());
        usefulMaterialBinding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        usefulMaterialBinding.recyclerView.setAdapter(adapter);
        usefulMaterialViewModel.data.observe(this, new Observer<MaterialData[]>() {
            @Override
            public void onChanged(MaterialData[] materialData) {
                adapter.setList(materialData);
            }
        });

    }


    @Override
    public void onItemClick(int position) {

    }
}
