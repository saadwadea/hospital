package bee.bee.astrazeneca.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;

public class Therapy extends ViewModel {
    public MutableLiveData<MaterialData[]> data=new MutableLiveData<>();

    public MaterialData[] getData() {
        MaterialData[] myListData = new MaterialData[] {
                new MaterialData(R.drawable.berlit_image,"BRILINTA (Ticagrelor)","BRILINTA (Ticagrelor) 90mg twice daily in combination with Acetylsalicylic acid (ASA) IS ..."),
                new MaterialData(R.drawable.berlit_image,"BRILINTA (Ticagrelor)","BRILINTA (Ticagrelor) 90mg twice daily in combination with Acetylsalicylic acid (ASA) IS ..."),
                new MaterialData(R.drawable.berlit_image,"BRILINTA (Ticagrelor)","BRILINTA (Ticagrelor) 90mg twice daily in combination with Acetylsalicylic acid (ASA) IS ..."),

        };
        data.setValue(myListData);
        return myListData;
    }
}
