package bee.bee.astrazeneca.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.RecordedVideoAdapter;
import bee.bee.astrazeneca.adapter.UsefulMaterialAdapter;
import bee.bee.astrazeneca.databinding.FragmentPrivacyBinding;
import bee.bee.astrazeneca.databinding.FragmentRecordedVideoBinding;
import bee.bee.astrazeneca.pojo.RecordedVideoData;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.viewmodels.RecordedVideo;
import bee.bee.astrazeneca.ui.viewmodels.UsefulMaterial;
import bee.bee.astrazeneca.util.AnimationExpandableList;


public class RecordedVideoFragment extends Fragment implements RecordedVideoAdapter.OnItemClickListener {
    MainActivity activity;
    FragmentRecordedVideoBinding binding;
    private RecordedVideo recordedVideo;
    private RecordedVideoAdapter adapter;
    private boolean swapDiabetes = true;
    private boolean swapEducational = true;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_recorded_video, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        recordedVideo = new ViewModelProvider(this).get(RecordedVideo.class);
        adapter = new RecordedVideoAdapter(activity, recordedVideo.getData());
        adapter.setOnItemClickListener(RecordedVideoFragment.this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerView.setAdapter(adapter);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.showToolbar();
        activity.setHeaderTitle(getString(R.string.Recorded_Videos));
        activity.hideBackImage();
        activity.showMenu();
        activity.showSearchImage();
        activity.changeColorToolbar(ContextCompat.getColor(activity, R.color.pink));
        activity.changeColorMenu(R.drawable.ic_menu_white_24dp);
        activity.changeColorSearch(R.drawable.icon_search_white);
        activity.changeColorHeaderTitle(ContextCompat.getColor(activity, R.color.white));
    }

    private void setListener() {


        recordedVideo.data.observe(getViewLifecycleOwner(), recordedVideoData -> adapter.setList(recordedVideoData));
        binding.diabetesContainer.setOnClickListener(v -> {
           // binding.expandableContainerDiabetes.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (binding.expandableContainerDiabetes.getVisibility()==View.GONE) {
                AnimationExpandableList.getInstance().expand(binding.expandableContainerDiabetes);
                binding.imageDiabetes.setImageResource(R.drawable.icons_cheveron_up);
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerEducational);
                binding.imageEducational.setImageResource(R.drawable.icons_chevron);
            } else {
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerDiabetes);
                binding.imageDiabetes.setImageResource(R.drawable.icons_chevron);

            }
        });
        binding.educationalContainer.setOnClickListener(v -> {
          //  binding.expandableContainerEducational.measure(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            if (binding.expandableContainerEducational.getVisibility()==View.GONE) {
                AnimationExpandableList.getInstance().expand(binding.expandableContainerEducational);
                binding.imageEducational.setImageResource(R.drawable.icons_cheveron_up);
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerDiabetes);
                binding.imageDiabetes.setImageResource(R.drawable.icons_chevron);
            } else {
                AnimationExpandableList.getInstance().collapse(binding.expandableContainerEducational);
                binding.imageEducational.setImageResource(R.drawable.icons_chevron);

            }
        });
    }

    @Override
    public void onItemClick(int position) {
        activity.replaceCurrentFragment(R.id.main_fragment,
                new RecordedVideoDetailsFragment(), "RecordedVideoDetailsFragment", true, true);
    }
}
