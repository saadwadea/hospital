package bee.bee.astrazeneca.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.InflateException;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.adapter.UsefulMaterialAdapter;
import bee.bee.astrazeneca.databinding.FragmentTherapyBinding;
import bee.bee.astrazeneca.ui.activities.MainActivity;
import bee.bee.astrazeneca.ui.viewmodels.Therapy;


public class TherapyFragment extends Fragment implements UsefulMaterialAdapter.OnItemClickListener {
    MainActivity activity;
    private FragmentTherapyBinding binding;
    UsefulMaterialAdapter adapter;
    Therapy therapyViewModel;
    float dX, dY;
    View v;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // must remove parent of view to make another layout inflating and avoid
        // duplicating fragment
        if (v != null) {
            ViewGroup parent = (ViewGroup) v.getParent();
            if (parent != null)
                parent.removeView(v);
        }
        try {
            binding = DataBindingUtil.inflate(inflater, R.layout.fragment_therapy, null, false);
            v = binding.getRoot();
        } catch (InflateException e) {

        }
        return v;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        therapyViewModel = new ViewModelProvider(this).get(Therapy.class);
        adapter = new UsefulMaterialAdapter(activity, therapyViewModel.getData());
        adapter.setOnItemClickListener(TherapyFragment.this);
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(activity));
        binding.recyclerView.setAdapter(adapter);
        setListener();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivity) {
            activity = (MainActivity) context;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        activity.hideToolbar();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void setListener() {
        getData();
        binding.imgViewMenuTherapy.setOnClickListener(v -> activity.openMenu());
        binding.search.setOnClickListener(v -> activity.replaceCurrentFragment(R.id.main_fragment,
                new SearchFragment(), "SearchFragment", true, true));
        binding.dragLayout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getActionMasked()) {
                    case MotionEvent.ACTION_DOWN:
                        dX = v.getX() - event.getRawX();
                        dY = v.getY() - event.getRawY();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        v.setY(event.getRawY() + dY);
                        v.setX(event.getRawX() + dX);
                        break;

                    default:
                        return false;
                }
                return true;
            }
        });
    }

    private void getData() {
        therapyViewModel.data.observe(getViewLifecycleOwner(), materialData -> adapter.setList(materialData));

    }

    @Override
    public void onItemClick(int position) {
        activity.replaceCurrentFragment(R.id.main_fragment,
                new BrilintaFragment(), "BrilintaFragment", true, true);
    }
}
