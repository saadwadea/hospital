package bee.bee.astrazeneca.ui.viewmodels;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

import bee.bee.astrazeneca.R;
import bee.bee.astrazeneca.pojo.MaterialData;

public class UsefulMaterial extends ViewModel {
   public MutableLiveData<MaterialData[]>data=new MutableLiveData<>();

    public MaterialData[] getData() {
        MaterialData[] myListData = new MaterialData[] {
                new MaterialData(R.drawable.materials,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.materials,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.materials,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.materials,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.materials,"Nexium Effective strategy - Oct","16 October 2019"),

        };
        data.setValue(myListData);
        return myListData;
    }
    public MaterialData[]getData2(){
        MaterialData[] myListData = new MaterialData[] {
                new MaterialData(R.drawable.gcc,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.gcc,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.gcc,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.gcc,"Nexium Effective strategy - Oct","16 October 2019"),
                new MaterialData(R.drawable.gcc,"Nexium Effective strategy - Oct","16 October 2019"),

        };
        data.setValue(myListData);
        return myListData;
    }
}